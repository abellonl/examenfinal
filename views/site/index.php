<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h5>Consulta 1</h5>
    <p>Los ciclistas cuya edad está entre 25 y 35 años mostrando únicamente el dorsal y el nombre del ciclista.</p>
    
    
    <h5>Consulta 2</h5>
    <p>Las etapas no circulares mostrando sólo el número de etapa y la longitud de las mismas.</p>
    
    
    <h5>Consulta 3</h5>
    <p>Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó.</p>
    
    
</div>
